package com.crm.automation.testing.application.udemy.models;

import lombok.Data;

@Data
public class User {

    private String name;
    private String job;
}
