package com.crm.automation.testing.application.udemy.input;


import lombok.Data;

@Data
public class UserInput {

    private String name;
    private String job;
}
