package com.crm.automation.testing.application.udemy.tools;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class NumberGenerator {

    public String nextAsString(){
        return RandomStringUtils.randomAlphabetic(7);
    }

    public Integer nextInteger(){
        return RandomUtils.nextInt(0,Integer.MAX_VALUE);
    }

    public Long nextLong(){
        return RandomUtils.nextLong( 0,Long.MAX_VALUE);
    }

}
