package com.crm.automation.testing.application.udemy.commons;

public enum CommonType {

    URL_PROYECTO("https://reqres.in/");

    private String key;

    CommonType(String key) {
        this.key=key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
